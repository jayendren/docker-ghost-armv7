FROM ubuntu
MAINTAINER jayendren@gmail.com

RUN apt-get update && \    
    apt-get install -yq --no-install-recommends \
	  unzip curl sqlite3 supervisor nginx build-essential

RUN mkdir -p /www
WORKDIR /www

RUN curl -O http://node-arm.herokuapp.com/node_latest_armhf.deb && \
    dpkg -i node_latest_armhf.deb && \
    rm -f node_latest_armhf.deb

RUN curl -L -k -O https://ghost.org/zip/ghost-latest.zip && \
    unzip -d ghost ghost-latest.zip  && \
    rm -f ghost-latest.zip

WORKDIR /www/ghost
RUN npm install --unsafe-perm sqlite3 && \
    npm install --unsafe-perm --production

WORKDIR /etc/nginx
RUN rm -f sites-enabled/default && \
    rm -f sites-available/default && \
    rm -f nginx.conf    
ADD src/nginx_ghost.conf sites-enabled/default
ADD src/nginx.conf nginx.conf

ADD src/supervisord-ghost.conf /etc/supervisor/conf.d/
ADD src/start.sh /start.sh
RUN chmod +x /start.sh 

RUN apt-get purge -yq build-essential && \
    apt-get autoremove -yq && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

EXPOSE 80

WORKDIR /www/ghost
CMD /start.sh